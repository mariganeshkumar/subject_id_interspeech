\documentclass[a4paper]{article}

\usepackage{INTERSPEECH2018}
\usepackage{cite}
\usepackage{slashbox}
\usepackage{multicol}
\usepackage[colorlinks,citecolor=blue,urlcolor=blue,pdftitle={Octopus},pdfauthor={Mari},pdfsubject={LaTeX}]{hyperref}
\usepackage{multirow}
\usepackage{array}
\usepackage[table]{xcolor}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{balance}
\DeclareMathOperator*{\argmin}{argmin}
\DeclareMathOperator*{\argmax}{argmax}
\newcommand*{\argminl}{\argmin\limits}
\newcommand*{\argmaxl}{\argmax\limits}
%\setlist[enumerate]{noitemsep, nolistsep}
\newcolumntype{P}[1]{>{\centering\arraybackslash}p{#1}}
\newcolumntype{M}[1]{>{\centering\arraybackslash}m{#1}}


\title{Task-Independent EEG based Subject Identification using Auditory Stimulus}
\name{Vinothkumar D.$^1$, Mari Ganesh Kumar$^1$,  Abhishek Kumar$^1$, Hitesh 
Gupta$^1$, Saranya M. S.$^1$, Mriganka Sur$^2$, Hema A. Murthy$^1$}
%The maximum number of authors in the author list is twenty. If the number of contributing authors is more than twenty, they should be listed in a footnote or in acknowledgement section, as appropriate.
\address{
  $^1$Department of Computer Science and Engineering, Indian Institute of 
  Technology Madras \\
  $^2$Department of Brain and Cognitive Sciences, Massachusetts Institute of Technology}
\email{\{vinoth, mari, abkumar, hiteshg, saranms\}@.iitm.ac.in, msur@mit.edu, hema@cse.iitm.ac.in}

\begin{document}

\maketitle
% 

\maketitle
% 
\begin{abstract}
Recent studies have shown that task-specific electroencephalography  (EEG) can be used as a reliable biometric. This paper extends this study to task-independent EEG with auditory stimuli. Data collected from $40$ subjects in response to various types of audio stimuli, using a $128$ channel EEG system is presented to different classifiers, namely,  k-nearest neighbor (k-NN),  artificial neural network (ANN) and universal background model - Gaussian mixture model (UBM-GMM).  It is observed that k-NN and ANN perform well when testing is performed intrasession, while UBM-GMM framework is more robust when testing is performed intersession.  This can be attributed to the fact that the correspondence of the sensor locations across sessions is only approximate. It is also observed that EEG from parietal and temporal regions contain more subject information although the performance using all the $128$ channel data is marginally better.
	
\end{abstract}
\noindent\textbf{Index Terms}: subject identification,  
electroencephalography, EEG biometrics, brain signals

\section{Introduction}
The objective of this paper is to extract signatures from EEG signals to uniquely recognize individuals. The study of subject-specific EEG signatures started with the analysis of genetic traits in twins \cite{doi:10.1001/archneurpsyc.1936.02260120061004} just 12 years after the first EEG recording on humans \cite{collura1993history}. The task of identifying subject singular signatures has its use in the following applications. The first one being the ability to use EEG signals as a biometric system to uniquely authenticate/recognize individuals. Biometric systems that use brain signals are difficult to spoof. For example, face detection can be spoofed by facemasks, fingerprints by gummy fingers, voice print by replayed audio, eye scanner by contact lenses and high-resolution photographs. In addition to the robustness of EEG based recognition systems against spoofing and stealing, it is impossible for an intruder to force a user to authenticate. The stress signals present in the measured brain waves will deny access \cite{Klonovs2013}. 


The task of subject identification (Subject ID) from EEG is similar to that of identifying a speaker from a speech signal where the phoneme information, language information, and speaker information are present. There are some studies in the literature \cite{Marcel2007, nguyen2015eeg, Ward2016} where ideas from speech processing have been exploited for EEG analyses. In speaker identification (Speaker ID), the objective is to remove the phone information while preserving the speaker information. Contradictory to this, in automatic speech recognition (ASR), the speaker-specific information is first discarded before processing the phone information \cite{POVEY2010, POVEY2011SG}. In EEG, irrespective of the task, information that discriminates among subjects  \cite{Berkhout1968, VANDIS197987} is present, due to morphological and functional plasticity traits. The ultimate objective with EEG is to separate the subject dependent, and independent signatures.  These subject-independent signatures could then be leveraged to build appropriate task-specific brain-computer interfaces (BCI).  


%Brain signals collected during specific cognitive functions or mental states, namely, imagined %speech, resting state, and responses to audio stimuli are considered in this study. 
%The first attempts at building EEG-based biometric system began in 1980 \cite{Poulos1998, %STASSEN1980} and the field has progressed substantially since then.

The main challenges to be tackled while modeling these subject-specific traits include: (i) the design of elicitation protocols that discriminate subjects and (ii) permanence of the measured signatures over time \cite{Campisi2014}. The commonly used elicitation protocols for EEG biometrics include resting state responses with open and closed eyes,  event-related potentials during some cognitive task. A detailed review of these  different elicitation protocols and their performance can be found in \cite{Campisi2014, DelPozoBanos2014}. Some of the previous works have shown subject identification across tasks \cite{Banos2015}, in which each trial unit consists of responses to a particular task.  Irrespective of the elicitation protocol, each of the results have shown success in subject identification, suggesting that any EEG signal must contain subject-specific traits.   The main issue is that the responses for different tasks may come from different brain areas. This makes task-independent subject identification a challenging problem.

The human brain is known to be highly plastic. Hence, the reproducibility of the subject-specific EEG signatures over multiple acquisitions is also critical. In addition to biometrics, in clinical applications, it is not desirable to have a significant variance between various acquisitions. This problem has been the objective of many studies in  clinical neurophysiology \cite{Berkhout1968, henry1941electroencephalographic1, henry1941electroencephalographic2, GASSER1985312, kondacs1999long, napflin2008test}.  These clinical studies suggest that alpha rhythms are more or less permanent. Despite these efforts,  the issue of repeatability has not yet received necessary attention from the engineering community working on EEG as a biometric \cite{Campisi2014}. \cite{Lee2013, la2013repeatability, Brigham2010, das2015} are some of the works that have reported the performance of EEG biometric with intersession repeatability.  Although they have reported reasonable accuracy on a small number of subjects, the variability across task is not analyzed, and the duration of the EEG signal required for subject identification is also not studied.

\begin{table*}[h]
	\centering
	\setlength{\tabcolsep}{2pt}
	\renewcommand{\arraystretch}{1.3}
	\caption{Details of data collection protocols.}
	\label{tab:exp_detials}
	\resizebox{0.95\textwidth}{!}{
		\begin{tabular}{|c|P{19mm}|p{100mm}|c|c|}
			\hline 
			\multirow{2}{*}{\bf No.} & \multirow{1}{*}{\bf Experiment} & \multirow{2}{*}{\bf \hspace{4.3cm} Description} & \multirow{1}{*}{\bf No.of} & \multirow{1}{*}{\bf Total Duration} \\ 
			& \multirow{1}{*}{\bf Names} & & \multirow{1}{*}{\bf Recordings} & \multirow{1}{*}{\bf (in minutes)} \\  \hline \hline
			%\multicolumn{1}{|P{4mm}|}{\textbf{No.}}  & 	
			%\multicolumn{1}{P{20mm}|}{\textbf{Experiment Name}} &  
			%\multicolumn{1}{c|}{\textbf{Description}}
			% &
			%\multicolumn{1}{p{15mm}|}{\textbf{No. of recordings}} & 
			%\multicolumn{1}{p{15mm}|}{\textbf{Total recording duration (minutes)}}\\  \hline
			$1$ & Odd ball paradigm with audio beeps & Audio beeps of two different 
			frequencies were played as target and non-target stimulus. The subjects 
			were expected to respond to target stimulus through mouse clicks.    
			&  $20$ & $265$\\ \hline
			$2$ & Familiar and unfamiliar words & The subjects were presented with 
			common words and uncommon words. They were expected to respond 
			with a mouse click on hearing a  familiar word. & $14$ &  $196$\\ \hline
			$3$ & Imagining binary answers & A set of 
			questions with the answer being either yes or no were presented to the 
			subjects. They were  asked to imagine the answer and then respond 
			with a mouse click. 
			Left click was used for positive responses and right click for 
			negative responses. & $19$ & $276$\\ 
			\hline
			$4$ & Semantically opposite words &  Semantically opposite words such 
			as ``yes" and ``no"  were played to the subject over 
			multiple trials. Subject was instructed to respond with left and right 
			mouse 
			clicks depending on the semantics of the word being played. & $11$ & $158$ 
			\\ 
			\hline
			$5$ & Passive listening & Subjects listened passively to a  variety of audio stimuli such as stories, music, and sounds that trigger attention (for example sirens and the scattering of glass). & $31$ & $364$\\
			\hline
			
		\end{tabular}}
	\end{table*}

The most widely used classifiers for identifying subjects include k-NN \cite{Palaniappan2007},  ANN \cite{Bao2009, Gui2014, Palaniappan2007} and support vector machines (SVM) \cite{Brigham2010, ARMSTRONG2015}.  Apart from this, inspired by speaker verification, UBM-GMM \cite{Marcel2007, Ward2016,  nguyen2013eeg, Ward2016b, Altahat2015, Davis2015} is also used for EEG subject recognition.  In \cite{Marcel2007}, the UBM-GMM system is evaluated across sessions in a verification setting with only $6$ subjects as clients. In the same study, as the features were concatenated from different channels, the performance degrades significantly for intersession testing. In other studies, the UBM-GMM system was not tested across different acquisitions. The objective of this paper is two-fold:
\begin{enumerate}[noitemsep, nolistsep]
\item Find a robust machine learning algorithm among ANN, UBM-GMM, and k-NN that scales well across sessions, and across tasks
\item Study the performance variability across different areas of the brain and duration of the EEG signal.
\end{enumerate}
 



The rest of the paper is organized in the following manner: Section~\ref{sec:data_collection} discusses the different protocols used for EEG data collection. Section~\ref{sec:features} describes the features and Section~\ref{sec:classifiers} describes the classifiers used to build the biometric system. The experimental setup is explained in Section~\ref{sec:experiments}. The results of subject identification along with the test for intersession repeatability is discussed in Section~\ref{sec:results_and_discussion}. Section~\ref{sec:comapre} compares EEG subject identification  with speaker identification. A post-mortem analysis of the UBM-GMM result is discussed in Section~\ref{sec:analysis}, followed by conclusions in Section~\ref{sec:conclusion}.




\section{Data collection}
\label{sec:data_collection}

EEG signals were recorded using a $128$ channel EEG system provided by Electrical Geodesics, Inc (EGI) \cite{egi}. For this task, $95$ recordings were obtained from 40 healthy subjects with their age ranging from $22$ to $38$.  $15$ of $40$ subjects came for multiple sessions of recordings. All the subjects were made to sit in an anechoic chamber, and were presented with audio stimuli. For all the recordings the subjects were asked to keep their eyes closed.

Different protocols of stimuli were used to collect the dataset. All the protocols had a minute of resting state before and after the presentation of the stimuli. After the initial resting state, different kinds of audio stimuli were played to the subject. Each of these experiments followed different protocols. Details of all the protocols are given in Table~\ref{tab:exp_detials}. The protocols 1 to 4 have a cognitive load and the subjects were expected to perform some action based on the stimulus. The subjects were required to perform either a mental (imagined speech) or a motor activity (mouse clicks). In protocol 5, the subjects were expected to passively listen to audio stimuli such as stories and music. The protocol 5 captures various mental state depending on the subject. These five different protocols were designed to cover a diverse set of tasks or mental states limiting to auditory stimuli.

The brain signals obtained from all the different experimental protocols are 
then divided uniformly into chunks. It is to be noted that, this division is 
independent of the cognitive state, such as, whether the subject is in resting 
state or listening to an audio stimulus or giving feedback through motor/mental 
activity. The details of how these chunks are used in our experiments can be 
found in Section~\ref{sec:experiments}.





\section{Features}
\label{sec:features}
Some of the commonly used features for EEG biometrics include autoregressive (AR) coefficients \cite{Poulos1998, la2013repeatability,  Brigham2010,  Bao2009} and spectral features \cite{Berkhout1968, Lee2013,Gui2014, palaniappan2002, Palaniappan2007,  napflin2007test}. While AR coefficients are the most widely used features, a slight change in the estimated coefficients can change the location of the root significantly in the {\it z}-domain. Raw power spectral density (PSD) features were used as features in this work.   Contrasting with speaker identification, the resolution of EEG as a function of frequency is assumed to be linear. As the range of EEG signals is about $50$ Hz, the full band consisting of frequency up to $50$ Hz was used initially. In the literature, it has been established that different bands correspond to different states/actions of the subject. Hence, different sub-bands based subject identification (alpha [$8$ - $13$ Hz], beta [$13$ - $30$ Hz] and gamma [$30$ - $50$ Hz])  is also performed in this study.


\section{Classifiers}
\label{sec:classifiers}




The UBM-GMM \cite{Marcel2007, reynolds2000speaker} framework is a probabilistic framework first proposed in speaker verification/recognition.   The UBM-GMM enables a common space in which the learned features can be represented.  The UBM-GMM with $M$ components is mixture model and is given by:
\begin{align}
p(\bar{\text{x}}|\lambda) &=\sum_{k=1}^M w_k 
\mathcal{N}(\bar{\text{x}}|\bar{\mu}_k,\mathbf{\Sigma}_k)
\end{align}
where $\bar{\text{x}}$ is the feature vector, $\mathcal{N}(\bar{\text{x}}|\bar{\mu}_k,\mathbf{\Sigma}_k) $ is Gaussian with mean $\bar{\mu}_k$ and covariance $\mathbf{\Sigma}_k$. $w_k$ is the weight associated with $k$-th Gaussian. The UBM is trained using feature vectors from all subjects, and all sessions.   Subject-specific models are then built by adapting the UBM to subject-specific data using maximum-a-posteriori (MAP) adaptation. All the channels/electrodes of the EEG signal are considered to be independent. Although in the literature \cite{Marcel2007, Altahat2015, Davis2015,nguyen2013eeg}, the feature vectors  are concatenated across channels, with 128 channels, concatenation of feature vectors could lead to overfitting owing to the paucity of data. During testing, the likelihood is normalized by performing a likelihood ratio test, given by Equation~\ref{eqn:30:ubmGmmScore}.  Here $\lambda_i$ is the subject-specific model, while $\lambda_{ubm} $ is the UBM-GMM model. $T$ is the number of feature vectors for a given length of time, and $C$ is the total number of channels in the EEG signal. The final decision is made using:

\begin{align}
	\begin{split}
	S_{\textrm{ID}}=\argmax_{i} \left(\frac{1}{C \times T} \sum_{c=1}^{C} \sum_{t=1}^{T }  \lbrace log \; P(\bar{\text{x}}_t^c|\lambda_{i}) \right. \\
	\left. - \; log \; P(\bar{\text{x}}_t^c|\lambda_{\textrm{UBM}}) \vphantom{\frac{1}{C \times T} \sum_{c=1}^{C} \sum_{t=1}^{T }} \rbrace \right)
	\end{split}
	\label{eqn:30:ubmGmmScore}
\end{align} 
For ANN, the PSD features were concatenated from all the channels and then used. During testing, the probabilities obtained from concatenated frames are averaged across all the frames of PSD to get the subject label. When each channel's feature vectors were considered to be independent, ANN did not converge. k-NN being a non-parametric method, the frames of the PSD were averaged to get an average spectrum, and then concatenated across channels. Similar to ANN, k-NN also failed to give good results without channel concatenation.


\section{Experimental setup}
\label{sec:experiments}


The experiments in this paper are designed to answer the following questions. 
\begin{enumerate}
	\item How conclusively can subjects be identified from EEG irrespective of 
	the task?
	\item Can the same subject-specific signatures be detected across multiple 
	sessions or acquisitions?
\end{enumerate} 

The first question is answered by taking all the 40 subjects from different data collection protocols in Table~\ref{tab:exp_detials}. The data collected from various protocols are divided uniformly into task-independent chunks. These chunks are then assumed as independent trials or data instances for the classification problem. These trials are randomly divided in the ratio 60:10:30 as train, validation, and test for building,  evaluating and testing the models respectively. It is to be noted that, training data is taken from all the sessions for each subject. This experiment will be referred to as  ``{\it classical testing}" in the following sections. To address the second question, 15 subjects with multiple session data are only considered. In this approach, the train and test data are split according to the number of sessions. The train and test sessions are chosen to be mutually exclusive  (no overlap). This experiment will be referred to as ``{\it intersession testing}" as the modeled signatures are tested across different sessions. The system performance is also evaluated for various chunk sizes. In Figure~\ref{fig:topoplot}.{\normalfont \textbf{A - C}} correspond to topographic maps of different trials in a single session of a subject. Similarly, Figure~\ref{fig:topoplot}.{\normalfont \textbf{D - F}} correspond to the maps of the same subject obtained from a different session. Figure~\ref{fig:topoplot}.{\normalfont \textbf{G - L}} belong to a different subject. From the figures, variability can be observed across trials, sessions, and subjects. Nevertheless, different trials and sessions from the same subject (comparing within \textbf{A - F} and \textbf{G - L}) are observed to be more similar, compared to that of the maps across subjects (comparing between \textbf{A - F} and \textbf{G - L}).

\begin{figure}[!ht]
	\centering
	\includegraphics[scale=0.16]{topoplot.pdf}
	\caption{Topographic plot of averaged beta band power for various trials and subjects. The trials {\normalfont \textbf{A} - \textbf{F}} belong to subject $1$, while trials {\normalfont \textbf{G} - \textbf{L}} belong to subject $2$. Sessions corresponds to different days of recording from the same subject.}
	\label{fig:topoplot}
\end{figure}

Another set of experiments were performed to determine the area of the brain that contains signatures that are more subject singular.

The classification accuracy is used as an evaluation metric to test the 
performance of the systems in all the experiments. Classification accuracy is 
the total percentage of trials for which the subjects were identified 
correctly.   


\section{Results and discussion}
\label{sec:results_and_discussion}




As discussed in Section~\ref{sec:experiments}, the dataset was initially divided into chunks of length $30$ and $70$ seconds. The result of {\it classical testing} with $40$ subjects is given in Table~\ref{tab:classic} and the same for {\it intersession testing} with $15$ subjects is given in Table~\ref{tab:intersession}. From Table~\ref{tab:classic}, it can be observed that in intrasession trials, ANN achieves a maximum accuracy of $100$\%,  when the chunk size is $70$s. The performance of k-NN is similar while that of UBM-GMM is relatively poor.   All the three classification results indicate that subject information is indeed present in EEG signals, especially for intrasession.


{\it Intersession testing} is a  harder problem as compared to the classical 
testing. Some factors such as placement of the electrodes, the time of the 
recording and the mental state of the subject could be different across sessions for the same subject. From Table~\ref{tab:intersession}, it can be inferred that the UBM-GMM based classifier is relatively more robust across sessions compared to ANN and k-NN.  The results in Table~\ref{tab:intersession} also indicate that there is a significant difference in performance across bands, with gamma showing the worst performance.


\begin{table}[h]
	\setlength{\tabcolsep}{2pt}
	\renewcommand{\arraystretch}{1.3}
	\textbf{}
	\caption{Accuracy (in \%) of classical testing on 40 subjects.}
	\resizebox{0.47\textwidth}{!}{
		\begin{tabular}{|c||*{6}{P{2.3em}|}}\hline
			\multirow{2}{*}{\backslashbox{Features}{Classifiers}}
			& \multicolumn{2}{c|} 
			{k-NN}&\multicolumn{2}{c|}{ANN}&\multicolumn{2}{c|}{UBM-GMM}
			\\ \cline{2-7}
			& {\it 30s} & {\it 70s} & {\it 30s} & {\it 70s} & {\it 30s} & {\it 
				70s}\\ \hline \hline
			PSD (All Bands) & 96.1 & 90.9 & 86.3 & 86.3 & 89.5 & 91.2 
			\\\hline
			PSD (Alpha Band) & 95.3 & 94.3 & 97.4 & 89.9 & 60.8 & 72.5
			\\\hline
			PSD (Beta Band) & 98.8 & 97.9 & 99.3 & 94.5 & 82.2 & 89.4
			\\\hline
			PSD (Gamma Band) & 98.5  & 95.1  & 99.9 & \textbf{100} & 51.0  
			& 55.9\\\hline
		\end{tabular}}
		\label{tab:classic}
	\end{table}
	
	\begin{table}[h]
		\setlength{\tabcolsep}{2pt}
		\renewcommand{\arraystretch}{1.3}
		\textbf{}
		\caption{Accuracy (in \%) of intersession testing on 15 subjects.}
		\resizebox{0.47\textwidth}{!}{
			\begin{tabular}{|c||*{6}{P{2.3em}|}}\hline
				\multirow{2}{*}{\backslashbox{Features}{Classifiers}}
				& \multicolumn{2}{c|} 
				{k-NN}&\multicolumn{2}{c|}{ANN}&\multicolumn{2}{c|}{UBM-GMM}
				\\ \cline{2-7}
				& {\it 30s} & {\it 70s} & {\it 30s} & {\it 70s} & {\it 30s} & {\it 
					70s}\\ \hline \hline
				PSD (All Bands)  & 44.6 & 42.7 & 67.5 &   73.8 & 74.5  & 78.6
				\\\hline
				PSD (Alpha Band) & 58.4 & 58.3 & 60.6 & 57.3 & 55.4 & 61.2
				\\\hline
				PSD (Beta Band)  & 81.4 &80.6 &  81.8 & 78.4 & 81.7 & \textbf{86.8} 
				\\\hline
				PSD (Gamma Band) & 59.3 & 57.3 & 66.2  & 67.9 & 35.1 & 39.8 \\\hline
			\end{tabular}}
			\label{tab:intersession}
		\end{table}

\begin{figure}[!ht]
	\centering
	\includegraphics[scale=0.3]{chunk_wise_ink.pdf}
	\caption{Accuracy of subject Identification for various duration of EEG signals}
	\label{fig:acc_plot}
\end{figure}	

The chunk size was varied from $10$s-$90$s with an increment of $10$s, to determine the optimal chunk size for subject ID. The results of all three Subject ID systems using PSD features from the beta band for different chunk sizes is shown in Figure~\ref{fig:acc_plot}. This analysis is performed only for {\it intersession testing}. Observe that the accuracy increases as the chunk size increases, for the UBM-GMM system.  It is also evident that the UBM-GMM system performs far better than the ANN and k-NN system across sessions, as the chunk size increases.

\begin{table}[h]
	\setlength{\tabcolsep}{2pt}
	\renewcommand{\arraystretch}{1.3}
	\textbf{}
	\caption{Accuracy (in \%) obtained using a subset of channels from different brain areas 
	for intersession testing.}
	\resizebox{0.47\textwidth}{!}{
	\begin{tabular}{|c|*{5}{P{3.8em}|}}\cline{1-6}
		
		&All Channels & Frontal Lobe  & Parietal Lobe & 
		Occipital Lobe & 
		Temporal Lobe  \\ 
		\hline
		UBM-GMM & 81.7 &71.8  & 79.4  &  74.7 & 76.1
		\\\hline
		
	\end{tabular}}
	\label{tab:lobewise}
\end{table}

The results discussed so far have used all the 128 channels. It is possible that these subject-specific traits are present actively in some particular area of the brain. Therefore, while adapting and evaluating the UBM-GMM system, only channels belonging to a specific lobe of the brain were used. The result of this evaluation is given in Table~\ref{tab:lobewise}. The results in Table~\ref{tab:lobewise} were computed for the chunk size of $30$s and beta band. From the Table~\ref{tab:lobewise}, although all channels perform better, the performance from parietal and temporal lobe is also high. The high performance of the temporal lobe may be attributed to the use of audio stimuli in all the protocols used for data collection. 



\section{Comparison with speaker identification}
\label{sec:comapre}
Although subject identification using EEG signals and speaker identification have 
many similarities, there are some important differences.  The 
first and the foremost difference is the sampling rate.   While a sampling rate of 8 kHz is used for speaker identification tasks, EEG uses a rate of 250 Hz. Hence for subject identification, a larger time window is required to estimate the PSD. Owing to the low sampling rate, longer duration of EEG signals are required to identify a subject 
accurately. This is also evident from Figure~\ref{fig:acc_plot}.


The channels/electrodes used to record EEG signals are very different from the channels/microphones used to record speech. Between sessions, the location of different electrodes on the scalp can vary. UBM-GMM is, therefore, a better framework compared to k-NN and ANN for subject identification, as it transforms every feature vector to a vector of posteriors in the UBM-GMM space, which is akin to transforming the feature vectors non-linearly.
\section{Analysis of the UBM-GMM system}
\label{sec:analysis}

The results in Table~\ref{tab:classic} and \ref{tab:intersession}, suggest that UBM-GMM based system performs well in both {\it classical} and {\it intersession testing}.  The EEG signatures vary significantly from subject to subject due to morphology and plasticity of the brain.  Since the UBM-GMM is trained independent of a specific subject, it should be able to model a subspace, from which new subject-specific models can be obtained via MAP adaptation. To verify this hypothesis, in the {\it classical testing} scenario ($40$ subjects), only a set of random $20$ subjects were used for training, and the remaining $20$ subjects were only used for adaptation.  The same was repeated for {\it intersession testing} experiment ($15$ subjects) by retaining random $8$ subjects only for adaptation. The result was validated across three folds of splits, and the same can be found in Table~\ref{tab:ubmadapt}.
\begin{table}[h]
	\setlength{\tabcolsep}{2pt}
	\renewcommand{\arraystretch}{1.3}
	\textbf{}
	\caption{Accuracy (in \%) of subjects used for UBM training (UBM subjects) vs subjects used only for MAP adaptation (Adaption only subjects).}
	\resizebox{0.47\textwidth}{!}{
	\begin{tabular}{|c|c|P{5.5em}|P{6em}|}\cline{3-4}
	
	\multicolumn{2}{c|}{}& UBM Subjects & 
	Adaptation Only Subjects \\ \hline
	\multirow{3}{*}{Classical Testing}& Split-1 & 80.2 & 88.2 \\ \cline{2-4}
	& Split-2 & 86.0 & 81.5 \\ \cline{2-4}
	& Split-3 & 78.5 & 87.4 \\ \hline
	\multirow{3}{*}{Intersession Testing}& Split-1 & 79.8 & 80.3  \\ \cline{2-4}
	& Split-2 & 78.0 & 84.4 \\ \cline{2-4}
	& Split-3 & 83.6 & 79.1 \\ \hline
	\end{tabular}}
	\label{tab:ubmadapt}
\end{table}

Observe that, in Table~\ref{tab:ubmadapt}, the classification accuracy is more or less stable, even when the subjects are not used to model the UBM. The UBM also models the task-dependent information and other noise in addition to the task-independent subject information. With sufficient number of subjects, this information can be removed using techniques similar to joint factor analysis (JFA) \cite{kenny2007joint} and {\it i}-vector \cite{dehak2011front}.




%\begin{figure}
%	\includegraphics[scale=0.15]{drawing_3.pdf}
%
%	\caption{Demonstration of how the different features help to discriminate 
%	between the subjects.}
%\end{figure}


\section{Conclusion}
\label{sec:conclusion}
Subject-specific signatures can be detected in EEG signals independent of the task or cognitive state. The objective is to use EEG as a biometric or as a BCI.  In the former, subject information can be used for authentication, while for the latter subject information must be filtered out to obtain signatures that discriminate mental states associated with tasks to build better brain-computer interfaces.
%\begin{itemize}
%	\item Collective information from all the channels seems to 
%	discriminate the subjects. 
%	\item ANNs performs better when training data is chosen from all the 
%	available session.
%	\item UBM-GMM with LFCC feature extraction technique works better than 
%	the other approaches when test sessions and training sessions are 
%	distinct.
%\end{itemize}
%----------------------------------------------------------------------------
%	REFERENCES
%----------------------------------------------------------------------------
\small{\bibliographystyle{IEEEtran}
	\bibliography{ian.bib}}

\end{document}
\grid
\grid
\grid
\grid
\grid
